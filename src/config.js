import Phaser from 'phaser'
import Preload from './scenes/Preload'
import Menu from './scenes/Menu'
import FirstLevel from './scenes/FirstLevel'
import SecondLevel from './scenes/SecondLevel'

export default {
    type: Phaser.auto,
    title: 'Game',
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 1200 }
        }
    },
    scene: [
        Preload,
        Menu,
        FirstLevel,
        SecondLevel
    ]
}
