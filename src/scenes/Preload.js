import Phaser from 'phaser'
import mapsprite from '../assets/env-sheet.png'
import menubg from '../assets/menu-bg.png'
import cell from '../assets/cell.png'

export default class Preload extends Phaser.Scene {
    constructor() {
        super({ key: 'preload' })
    }

    preload() {
        console.log('Preload:preload')
        const bg = this.add.rectangle(400, 300, 400, 30, 0x666666)
        const bar = this.add.rectangle(bg.x, bg.y, bg.width, bg.height, 0xffffff).setScale(0, 1)

        this.load.spritesheet('mapsprite', mapsprite, { frameWidth: 16, frameHeight: 16 })
        this.load.spritesheet('cell', cell, { frameWidth: 6, frameHeight: 6 })
        this.load.image('menubg', menubg)

        this.load.on('progress', progress => bar.setScale(progress, 1))
    }

    update() {
        console.log('Preload:update')
        this.scene.start('menu')
    }
}
