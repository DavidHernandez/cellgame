import Phaser from 'phaser'
import BaseLevel from './BaseLevel'

import Jump from '../controls/Jump'
import MoveOrRunLeft from '../controls/MoveOrRunLeft'
import MoveOrRunRight from '../controls/MoveOrRunRight'

export default class SecondLevel extends BaseLevel {
    constructor() {
        super({ key: 'second_level' })
        this.mapScale = 3
        this.playerInitialPosition = { x: 70, y: 287 }
        this.finishLine = { x: 715, y: 462 }
        this.nextLevel = 'menu'
    }

    get map() {
        return [
          [13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13],
          [ 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2],
          [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8],
          [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8],
          [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8],
          [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8],
          [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8],
          [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8],
          [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8],
          [ 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8],
          [12,10,10,10,10,10,10,10,10,10,10,10,10,10,10,15,11],
          [13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13],
          [13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13],
          [13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13],
        ]
    }

    get controls() {
        return [
            new MoveOrRunLeft(this.cursors, this.player, this.input.keyboard),
            new MoveOrRunRight(this.cursors, this.player, this.input.keyboard),
            new Jump(this.cursors, this.player),
        ]
    }

    get positionalTexts() {
        if (this.howToMoveText === undefined) {
            this.howToMoveText = this.addMessageOnLocation('Use the arrows to move', {x: 60, y: 220}, {x: 60, y: 100})
            this.howToJumpText = this.addMessageOnLocation('Use the up arrow to jump', {x: 140, y: 220}, {x: 60, y: 100})
            this.howToRunText = this.addMessageOnLocation('Double tap an arrow to run', {x: 400, y: 220}, {x: 130, y: 100})
        }
        return [
            this.howToMoveText,
            this.howToJumpText,
            this.howToRunText,
        ]
    }
}
