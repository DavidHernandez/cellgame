import Phaser from 'phaser'

import ClickableText from '../game_objects/ClickableText'
import StaticText from '../game_objects/StaticText'

export default class Menu extends Phaser.Scene {
    constructor() {
        super({ key: 'menu' })
        this.bindFunctions()
    }

    bindFunctions() {
        this.startGame = this.startGame.bind(this)
    }

    create() {
        this.add.image(400, 300, 'menubg')

        const textConfig = {
            align: 'center',
            fill: 'white',
            fontFamily: 'sans-serif',
            fontSize: 48
        }

        const title = new StaticText(this, 'Cell game', {x: 300, y: 200}, textConfig)
        this.add.existing(title)

        const play = new ClickableText(this, 'Play', {x: 300, y: 250}, this.startGame)
        this.add.existing(play)
    }

    startGame() {
        this.scene.start('first_level')
    }
}
