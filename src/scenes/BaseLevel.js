import Phaser from 'phaser'

import PositionalText from '../game_objects/PositionalText'
import StaticText from '../game_objects/StaticText'

const CELL = 'cell'
const MAP = 'mapsprite'
const BG = 0
const B = 1
const BL = 2
const BR = 3
const CBL = 4
const CBR = 5
const CTL = 6
const CTR = 7
const L = 8
const R = 9
const T = 10
const TL = 11
const TR = 12
const F = 13

export default class BaseLevel extends Phaser.Scene {
    constructor(data) {
        super(data)

        this.player
        this.platforms
        this.playerInitialPosition
        this.cursors
        this.finishLine
        this.mapScale = 1
    }

    get map() {
        return []
    }

    get controls() {
        return []
    }

    get positionalTexts() {
        return []
    }

    activateCursors() {
        this.cursors = this.input.keyboard.createCursorKeys()
    }

    startControls() {
        let isStopped = true
        for (const control of this.controls) {
            if (control.applies()) {
                isStopped = false
                control.execute()
            }
        }

        if (isStopped) {
            this.player.setVelocityX(0)
        }
    }

    createMap(mapMatrix) {
        this.platforms = this.physics.add.staticGroup()
        for (const y in mapMatrix) {
            const row = mapMatrix[y]
            for (const x in row) {
                const mapSprite = row[x]
                if (mapSprite <= BG) {
                    continue
                }
                const xPosition = this.calculateCoordinate(x)
                const yPosition = this.calculateCoordinate(y)
                this.platforms.create(xPosition, yPosition, MAP, mapSprite).setScale(this.mapScale).refreshBody()
            }
        }
    }

    calculateCoordinate(coordinate) {
        return coordinate * 16 * this.mapScale + (8 * this.mapScale)
    }

    createPlayer() {
        this.player = this.physics.add.sprite(this.playerInitialPosition.x, this.playerInitialPosition.y, CELL).setScale(this.mapScale)
        this.player.setBounce(0.2)
        this.player.setCollideWorldBounds(true)

        this.anims.create({
            key: 'normal',
            frames: this.anims.generateFrameNumbers(CELL, { start: 0, end: 11 }),
            frameRate: 20,
            repeat: -1
        })

        this.player.anims.play('normal', true)
    }

    playerDies() {
        this.scene.start('menu')
    }

    addMessageOnLocation(text, position, area) {
        const message = new PositionalText(this, text, position, area)

        this.add.existing(message)

        return message
    }

    addStaticText(text, position) {
        const message = new StaticText(this, text, position)

        this.add.existing(message)

        return message
    }

    updatePositionalTexts(position) {
        for (const text of this.positionalTexts) {
            text.updateState(position)
        }
    }

    create() {
        this.add.image(400, 300, 'menubg')
        this.createMap(this.map, 3)
        this.createPlayer()
        this.physics.add.collider(this.player, this.platforms)

        this.activateCursors()
        const position = this.player.body.position

        this.playerPositionText = this.addStaticText('X:' + position.x + ' Y:' + position.y, {x: 0, y: 0})

        this.positionalTexts
    }

    update() {
        this.startControls()

        const position = this.player.body.position

        if (position.y > 580) {
            this.playerDies()
        }

        this.updatePositionalTexts(position)
        this.playerPositionText.setText('X:' + position.x + ' Y:' + position.y)

        if (position.x > this.finishLine.x && position.y == this.finishLine.y) {
            this.scene.start(this.nextLevel)
        }
    }
}

