import Phaser from 'phaser'

export default class ClickableText extends Phaser.GameObjects.Text {
    constructor(scene, text, position, onClickCallback) {
        const textConfig = {
            align: 'center',
            fill: 'white',
            fontFamily: 'sans-serif',
            fontSize: 48
        }

        super(scene, position.x, position.y, text, textConfig)

        this.setInteractive()

        this.on('pointerdown', onClickCallback)
    }
}

