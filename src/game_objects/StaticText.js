import Phaser from 'phaser'

export default class StaticText extends Phaser.GameObjects.Text {
    constructor(scene, text, position, style = { fontsize: '16px', fill: '#FFF' }) {
        super(scene, position.x, position.y, text, style)
    }
}
