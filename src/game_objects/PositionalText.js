import Phaser from 'phaser'

export default class PositionalText extends Phaser.GameObjects.Text {
    constructor(scene, text, showOnPosition, showOnArea = { x: 40, y: 40 }) {
        super(scene, 250, 200, text, { fontSize: '16px', fill: '#FFF' })
        this.setVisible(false)

        this.position = showOnPosition
        this.size = showOnArea
    }

    updateState(position) {
        this.setVisible(this.isBetweenArea(position))
    }

    isBetweenArea(position) {
        return this.isBetweenAxis(position, 'x') && this.isBetweenAxis(position, 'y')
    }

    isBetweenAxis(position, axis) {
        const objectAxisPosition = position[axis]
        const minAxisPosition = this.position[axis]
        const maxAxisPosition = minAxisPosition + this.size[axis]

        return this.isBetween(objectAxisPosition, minAxisPosition, maxAxisPosition)
    }

    isBetween(number, min, max) {
        return number > min && number < max
    }
}
