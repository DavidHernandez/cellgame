let isRunning = false

export default class MoveOrRunLeft {
    constructor(cursors, player, keyboard) {
        this.cursors = cursors
        this.player = player

        keyboard.createCombo([37, 37], { resetOnMatch: true });
        keyboard.on('keycombomatch', (event, keyEvent) => {
            if (keyEvent.key == 'ArrowLeft') {
                isRunning = true
            }
        })

        keyboard.on('keyup-LEFT', (event) => {
            if (isRunning) {
                isRunning = false
            }
        })
    }

    applies() {
        return this.cursors.left.isDown
    }

    execute() {
        if (isRunning) {
            this.player.setVelocityX(-200)
        }
        else {
            this.player.setVelocityX(-130)
        }
    }
}
