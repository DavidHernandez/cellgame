let isRunning = false

export default class MoveOrRunRight {
    constructor(cursors, player, keyboard) {
        this.cursors = cursors
        this.player = player

        keyboard.createCombo([39, 39], { resetOnMatch: true });
        keyboard.on('keycombomatch', (event, keyEvent) => {
            if (keyEvent.key == 'ArrowRight') {
                isRunning = true
            }
        })

        keyboard.on('keyup-RIGHT', (event) => {
            if (isRunning) {
                isRunning = false
            }
        })
    }

    applies() {
        return this.cursors.right.isDown
    }

    execute() {
        if (isRunning) {
            this.player.setVelocityX(200)
        }
        else {
            this.player.setVelocityX(130)
        }
    }
}
