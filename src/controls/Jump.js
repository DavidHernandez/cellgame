export default class Jump {
    constructor(cursors, player) {
        this.cursors = cursors
        this.player = player
    }

    applies() {
        return this.cursors.up.isDown && this.player.body.touching.down
    }

    execute() {
        this.player.setVelocityY(-350)
    }
}
